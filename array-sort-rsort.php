<?php
$arrNilai=array("Asti"=>70,"Linda"=>90,"Rizka"=>75,"Amir"=>85);
echo "<b>array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan asort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan arsort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>