<?php
$arrNilai=array("Asti"=>70,"Linda"=>60,"Rizka"=>90,"Amir"=>75);
echo "<b>array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

asort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan sort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

arsort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan rsort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>