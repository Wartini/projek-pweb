<?php
$arrNilai=array("Asti"=>70,"Linda"=>60,"Rizka"=>90,"Amir"=>75);
echo "<b>array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

ksort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan ksort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

krsort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan krsort</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>